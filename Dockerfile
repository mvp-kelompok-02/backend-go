FROM golang 
WORKDIR /go/src/app
COPY . .

ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64

RUN go get -d -v ./...
RUN go install -v ./...
RUN go build -o main .

FROM scratch

WORKDIR /dist
COPY --from=0 /go/src/app/main .
COPY --from=0 /go/src/app/templates ./templates
COPY --from=0 /go/src/app/templates ./app/templates
EXPOSE 4121

CMD ["./main"]