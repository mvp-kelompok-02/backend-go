package controller

import (
	"bytes"
	"fmt"
	"mime/quotedprintable"
	"mvp-backend/app/constant"
	"mvp-backend/app/model"
	"mvp-backend/app/utils"
	"net/http"
	"net/smtp"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
)

// // smtpServer data to smtp server
// type smtpServer struct {
// 	host string
// 	port string
// }

// // Address URI to smtp server
// func (s *smtpServer) Address() string {
// 	return s.host + ":" + s.port
// }

//Register
func Register(c *gin.Context) {
	var account model.SignUp

	if err := c.Bind(&account); err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}

	if account.Password != account.ConfirmPassword {
		utils.WrapAPIData(c, map[string]interface{}{
			"Error": "Password Not Match",
		}, http.StatusOK, "success")
		return
	}

	pass, err := utils.HashGenerator(account.Password)
	if err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
	account.Password = pass

	t := time.Now()
	t.String()
	account.CreatedAt = t.Format("2006-01-02 15:04:05")
	account.Role = constant.MEMBER
	account.IsVerified = constant.ISVERIFIED
	account.Flag = constant.NORMAL

	//Cek Duplicate Email
	flag := model.Registered(account)
	if flag == false {
		utils.WrapAPIData(c, map[string]interface{}{
			"Error": "Email Was Registered !",
		}, http.StatusOK, "success")
		return
	}

	//If Email Not Duplicate
	flag, er, id := model.Register(account)
	if flag {
		//TODO: Pindahin ke Models
		//Send To email

		//FIXME:

		fromEmail := "digitalent.cocreate@gmail.com"
		password := "kelompok2"
		host := "smtp.gmail.com:587"
		auth := smtp.PlainAuth("", fromEmail, password, "smtp.gmail.com")

		header := make(map[string]string)
		toEmail := account.Email
		header["From"] = fromEmail
		header["To"] = toEmail
		header["Subject"] = "CoCreate Email Verification"

		header["MIME-Version"] = "1.0"
		header["Content-Type"] = fmt.Sprintf("%s; charset=\"utf-8\"", "text/html")
		header["Content-Transfer-Encoding"] = "quoted-printable"
		header["Content-Disposition"] = "inline"

		headerMessage := ""
		for key, value := range header {
			headerMessage += fmt.Sprintf("%s: %s\r\n", key, value)
		}

		//Body Message
		// body := " <h1>Selamat Datang di CoCreate</h1><p>Verifikasi akun anda dengan klik <a href= 'http://localhost:4121/account/verification/email/" + id + "'>disini</a></p>"
		body := " <h1>Selamat Datang di CoCreate</h1><p>Verifikasi akun anda dengan klik <a href= 'http://54.214.195.229:80/account/verification/email/" + id + "'>disini</a></p>"
		var bodyMessage bytes.Buffer
		temp := quotedprintable.NewWriter(&bodyMessage)
		temp.Write([]byte(body))
		temp.Close()

		finalMessage := headerMessage + "\r\n" + bodyMessage.String()
		status := smtp.SendMail(host, auth, fromEmail, []string{toEmail}, []byte(finalMessage))
		if status != nil {
			utils.WrapAPIData(c, map[string]interface{}{
				"account": status,
				"erro":    errors.Errorf("invalid prepare statement :%+v\n"),
			}, http.StatusOK, "success")

			utils.WrapAPIError(c, er.Error(), http.StatusBadRequest)
			return
		}

		//FIXME:

		// const CONFIG_SMTP_HOST = "smtp.gmail.com"
		// const CONFIG_SMTP_PORT = 587
		// const CONFIG_SENDER_NAME = "CoCreate DigiTalent <digitalent.cocreate@gmail.com>"
		// const CONFIG_AUTH_EMAIL = "digitalent.cocreate@gmail.com"
		// const CONFIG_AUTH_PASSWORD = "kelompok2"

		// mailer := gomail.NewMessage()
		// mailer.SetHeader("From", CONFIG_SENDER_NAME)
		// mailer.SetHeader("To", account.Email)
		// // mailer.SetAddressHeader("Cc", "arcen.alter@gmail.com", "Tra Lala La")
		// mailer.SetHeader("Subject", "Konfirmasi Email")
		// mailer.SetBody("text/html", "<h1>Selamat Datang di CoCreate</h1><p>Verifikasi akun anda dengan klik <a href= 'http://54.214.195.229:80/account/verification/email/"+id+"'>disini</a></p>")
		// // mailer.Attach("./sample.png")

		// dialer := gomail.NewDialer(
		// 	CONFIG_SMTP_HOST,
		// 	CONFIG_SMTP_PORT,
		// 	CONFIG_AUTH_EMAIL,
		// 	CONFIG_AUTH_PASSWORD,
		// )

		// err := dialer.DialAndSend(mailer)
		// if err != nil {
		// 	log.Fatal(err.Error())
		// }

		// log.Println("Mail sent!")

		//FIXME:
		// apiUrl := "https://agree-ai.com"
		// resource := "/mail/index.php/mail"
		// data := url.Values{}
		// data.Set("pengirim", "dtstaftui@agree-ai.com")
		// data.Set("penerima", "arcen.grizzly@gmail.com")
		// data.Set("isi", "data.Isi")
		// data.Set("judul", "data.Judul"+id)

		// u, _ := url.ParseRequestURI(apiUrl)
		// u.Path = resource
		// urlStr := u.String()

		// client := &http.Client{}
		// r, _ := http.NewRequest(http.MethodPost, urlStr, strings.NewReader(data.Encode())) // URL-encoded payload
		// // r.Header.Add("Authorization", "auth_token=\"XXXXXXX\"")
		// r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
		// r.Header.Add("X-API-KEY", "dtstaftui")
		// r.Header.Add("Content-Length", strconv.Itoa(len(data.Encode())))

		// resp, err := client.Do(r)
		// if err != nil {
		// 	utils.WrapAPIData(c, map[string]interface{}{
		// 		"accoun3t": err,
		// 		// "erro":    errors.Errorf("invalid prepare statement :%+v\n"),
		// 	}, http.StatusOK, "success")
		// 	return
		// }
		// fmt.Println(resp.Status)

		//FIXME:
		// var err error
		// var client = &http.Client{}
		// var data verification

		// var param = url.Values{}
		// param.Set("pengirim", "dtstaftui@agree-ai.com")
		// param.Set("penerima", "arcen.grizzly@gmail.com")
		// param.Set("isi", "data.Isi")
		// param.Set("judul", "data.Judul"+id)
		// var payload = bytes.NewBufferString(param.Encode())

		// request, err := http.NewRequest("POST", "https://agree-ai.com/mail/index.php/mail", payload)
		// if err != nil {
		// 	utils.WrapAPIData(c, map[string]interface{}{
		// 		"account1": err,
		// 		// "erro":    errors.Errorf("invalid prepare statement :%+v\n"),
		// 	}, http.StatusOK, "success")
		// 	return
		// }
		// request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		// request.Header.Set("X-API-KEY", "dtstaftui")

		// response, err := client.Do(request)
		// if err != nil {
		// 	utils.WrapAPIData(c, map[string]interface{}{
		// 		"account2": err,
		// 		// "erro":    errors.Errorf("invalid prepare statement :%+v\n"),
		// 	}, http.StatusOK, "success")
		// 	return
		// }
		// defer response.Body.Close()

		// err = json.NewDecoder(response.Body).Decode(&data)
		// if err != nil {
		// 	utils.WrapAPIData(c, map[string]interface{}{
		// 		"accoun3t": err,
		// 		// "erro":    errors.Errorf("invalid prepare statement :%+v\n"),
		// 	}, http.StatusOK, "success")
		// 	return
		// }

		//FIXME:
		// cmdStr := "sudo docker run --rm msmtp-test bash -c 'printf \"Subject: Test from msmtp\nThis is a test\" | msmtp arcen.grizzly@gmail.com'"
		// out, _ := exec.Command("/bin/sh", "-c", cmdStr).Output()

		// if err != nil {
		// 	// panic(err)
		// 	utils.WrapAPIData(c, map[string]interface{}{
		// 		"accoun3t": err,
		// 		// "erro":    errors.Errorf("invalid prepare statement :%+v\n"),
		// 	}, http.StatusOK, "success")
		// 	return
		// }
		// utils.WrapAPIData(c, map[string]interface{}{
		// 	"aman": out,
		// 	// "erro":    errors.Errorf("invalid prepare statement :%+v\n"),
		// }, http.StatusOK, "success")
		// return

		//FIXME:
		// cli, err := client.NewEnvClient()
		// if err != nil {
		// 	// panic(err)
		// 	utils.WrapAPIData(c, map[string]interface{}{
		// 		"accoun3t": err,
		// 		// "erro":    errors.Errorf("invalid prepare statement :%+v\n"),
		// 	}, http.StatusOK, "success")
		// 	return
		// }

		// ctx := context.Background()
		// resp, err := cli.ContainerCreate(ctx, &container.Config{
		// 	Image:        "msmtp-test",
		// 	ExposedPorts: nat.PortSet{"8080": struct{}{}},
		// }, &container.HostConfig{
		// 	PortBindings: map[nat.Port][]nat.PortBinding{nat.Port("8080"): {{HostIP: "127.0.0.1", HostPort: "8080"}}},
		// }, nil, "mongo-go-cli")
		// if err != nil {
		// 	utils.WrapAPIData(c, map[string]interface{}{
		// 		"accoun2t": err,
		// 		// "erro":    errors.Errorf("invalid prepare statement :%+v\n"),
		// 	}, http.StatusOK, "success")
		// }

		// if err := cli.ContainerStart(ctx, resp.ID, types.ContainerStartOptions{}); err != nil {
		// 	utils.WrapAPIData(c, map[string]interface{}{
		// 		"accoun1t": err,
		// 		// "erro":    errors.Errorf("invalid prepare statement :%+v\n"),
		// 	}, http.StatusOK, "success")
		// }

		utils.WrapAPISuccess(c, "success, check your email to verified !", http.StatusOK)
	} else {
		utils.WrapAPIError(c, er.Error(), http.StatusBadRequest)
		return
	}
}

type verification struct {
	Pengirim string
	Penerima string
	Isi      string
	Judul    string
}

func Login(c *gin.Context) {
	var auth model.Auth
	if err := c.Bind(&auth); err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
	flag, err, token, role, is_verified := model.Login(auth)
	if flag {
		utils.WrapAPIData(c, map[string]interface{}{
			"token":       token,
			"role":        role,
			"is_verified": is_verified,
		}, http.StatusOK, "success")

	} else {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
	}
}

// func Verification(c *gin.Context) {
// 	id := c.Param("id")
// 	utils.WrapAPIData(c, map[string]interface{}{
// 		"account": id,
// 	}, http.StatusOK, "success")
// }

func Index(c *gin.Context) {
	utils.WrapAPIData(c, map[string]interface{}{
		"message": "online",
	}, http.StatusOK, "success")
}
