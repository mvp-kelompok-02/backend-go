package controller

import (
	"log"
	"mvp-backend/app/model"
	"mvp-backend/app/utils"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func AccountProfile(c *gin.Context) {
	account := model.UserAccount{UserID: int(c.MustGet("user_id").(int))}
	flag, err, acc := model.AccountProfile(account)
	if err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusInternalServerError)
		return
	}
	flag, err, cat := model.GetCategoryID(account)
	if err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusInternalServerError)
		return
	}
	if flag {
		utils.WrapAPIData(c, map[string]interface{}{
			"account":  acc,
			"category": cat,
		}, http.StatusOK, "success")
		return
	}
}

func AllAccount(c *gin.Context) {
	role := c.MustGet("role").(int)
	if role != 1 && role != 0 {
		c.JSON(200, gin.H{
			"code":    "403",
			"message": "Not Permit",
			"status":  "success",
		})
		c.Abort()
		return
	}

	flag, err, acc := model.AllAccount()
	if err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusInternalServerError)
		return
	}
	if flag {
		utils.WrapAPIData(c, map[string]interface{}{
			"account": acc,
		}, http.StatusOK, "success")
		return
	}
}

func FilterAccount(c *gin.Context) {
	role := c.MustGet("role").(int)
	if role != 1 && role != 0 {
		c.JSON(200, gin.H{
			"code":    "403",
			"message": "Not Permit",
			"status":  "success",
		})
		c.Abort()
		return
	}

	f := c.Param("filter")
	i := c.Param("id")

	statusID, err := strconv.Atoi(i)
	if err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusInternalServerError)
	}
	var filter model.Filter
	filter.FilterName = f
	filter.FilterID = statusID

	flag, err, acc := model.FilterAccount(filter)
	if err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusInternalServerError)
		return
	}
	if flag {
		utils.WrapAPIData(c, map[string]interface{}{
			"account": acc,
		}, http.StatusOK, "success")
		return
	}
}

func EditAccount(c *gin.Context) {

	var userAccount model.UserAccount
	userId := c.MustGet("user_id").(int)

	if err := c.Bind(&userAccount); err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
	userAccount.UserID = userId

	//Check Email Same or Defferent
	flag, err := model.CheckEmail(userAccount, "edit")
	if err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusInternalServerError)
		return
	}
	if flag == false {
		//If Email Different, check from global email
		utils.WrapAPIData(c, map[string]interface{}{
			"Error": "Email Was Registered !",
		}, http.StatusOK, "success")
		return
	}

	flag, er := model.EditAccount(userAccount)
	if er != nil {
		utils.WrapAPIError(c, er.Error(), http.StatusInternalServerError)
		return
	}
	if flag {
		utils.WrapAPIData(c, map[string]interface{}{}, http.StatusOK, "success")
		return
	}

}

func AccountVerification(c *gin.Context) {
	role := c.MustGet("role").(int)
	if role != 1 && role != 0 {
		c.JSON(200, gin.H{
			"code":    "403",
			"message": "Not Permit",
			"status":  "success",
		})
		c.Abort()
		return
	}

	id := c.Param("id")

	userID, err := strconv.ParseInt(id, 10, 10)
	if err != nil {
		log.Fatalln(err)
	}
	account := model.UserAccount{UserID: int(userID)}
	flag, err := model.AccountVerification(account)
	if err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusInternalServerError)
		return
	}
	if flag {
		utils.WrapAPIData(c, map[string]interface{}{
			"action": "success",
		}, http.StatusOK, "success")
		return
	}
}

func AccountEmailVerification(c *gin.Context) {
	id := c.Param("id")

	userID, err := strconv.ParseInt(id, 10, 10)
	if err != nil {
		log.Fatalln(err)
	}
	account := model.UserAccount{UserID: int(userID)}
	flag, err := model.AccountVerification(account)
	if err != nil {
		c.HTML(http.StatusOK, "index.html", gin.H{
			"content": "Something wrong",
		})
		return
	}
	if flag {
		c.HTML(http.StatusOK, "index.html", gin.H{
			"content": "YOur Account Is Activated !",
		})
		return
	}
}

func VerificationMany(c *gin.Context) {

	var f model.UserMany
	if err := c.BindJSON(&f); err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusInternalServerError)
		return
	}
	flag, er := model.VerificationMany(f)
	if er != nil {
		utils.WrapAPIError(c, er.Error(), http.StatusInternalServerError)
		return
	}
	if flag {
		utils.WrapAPIData(c, map[string]interface{}{}, http.StatusOK, "success")
		return
	}
}

func ChangePassword(c *gin.Context) {
	var pass model.Password
	userID := c.MustGet("user_id").(int)

	if err := c.Bind(&pass); err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
	pass.UserID = userID

	if pass.NewPassword != pass.ConfirmPassword {
		c.JSON(200, gin.H{
			"code":    "401",
			"message": "Password Not Match",
			"status":  "Unauthorized",
		})
		c.Abort()
		return
	}

	flag, err := model.ChangePassword(pass)
	if err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusInternalServerError)
		return
	}
	if flag {
		utils.WrapAPIData(c, map[string]interface{}{}, http.StatusOK, "success")
		return
	}
}

func DeleteAccount(c *gin.Context) {
	role := c.MustGet("role").(int)
	if role != 1 && role != 0 {
		c.JSON(200, gin.H{
			"code":    "403",
			"message": "Not Permit",
			"status":  "success",
		})
		c.Abort()
		return
	}

	userId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusInternalServerError)
	}
	var user model.UserAccount
	user.UserID = userId

	flag, err := model.DeleteAccount(user)
	if err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusInternalServerError)
		return
	}
	if flag {
		utils.WrapAPIData(c, map[string]interface{}{}, http.StatusOK, "success")
	}
}

func ChooseUserCategory(c *gin.Context) {
	var cc model.ChooseCategory
	if err := c.BindJSON(&cc); err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusInternalServerError)
		return
	}

	userID := c.MustGet("user_id").(int)
	cc.UserID = userID

	flag, er := model.ChooseUserCategory(cc)
	if er != nil {
		utils.WrapAPIError(c, er.Error(), http.StatusInternalServerError)
		return
	}
	if flag {
		utils.WrapAPIData(c, map[string]interface{}{}, http.StatusOK, "success")
		return
	}
}

func ValidateEmail(c *gin.Context) {
	var userAccount model.UserAccount

	if err := c.Bind(&userAccount); err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}

	flag, err := model.CheckEmail(userAccount, "validate")
	if err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusInternalServerError)
		return
	}
	if flag == true {
		//If Email Different, check from global email
		utils.WrapAPIData(c, map[string]interface{}{
			"Email": "True",
		}, http.StatusOK, "success")
		return
	} else {
		utils.WrapAPIData(c, map[string]interface{}{
			"Email": "False",
		}, http.StatusOK, "success")
		return
	}
}
