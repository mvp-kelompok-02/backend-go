package controller

import (
	"mvp-backend/app/model"
	"mvp-backend/app/utils"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

func AddGroup(c *gin.Context) {
	var group model.GroupPI

	if err := c.BindJSON(&group); err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusInternalServerError)
		return
	}

	t := time.Now()
	t.String()
	group.CreatedAt = t.Format("2006-01-02 15:04:05")
	group.UserID = c.MustGet("user_id").(int)
	flag, err := model.AddGroup(group)
	if flag {
		utils.WrapAPISuccess(c, "success", http.StatusOK)
		return
	} else {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
}

func AddGroupMember(c *gin.Context) {
	var groupMember model.GroupMember
	if err := c.BindJSON(&groupMember); err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusInternalServerError)
		return
	}

	t := time.Now()
	t.String()
	groupMember.GroupID, _ = strconv.Atoi(c.Param("id"))
	groupMember.CreatedAt = t.Format("2006-01-02 15:04:05")

	flag, err := model.AddGroupMember(groupMember)
	if flag {
		utils.WrapAPISuccess(c, "success", http.StatusOK)
		return
	} else {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
}
