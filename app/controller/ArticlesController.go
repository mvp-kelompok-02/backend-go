package controller

import (
	"mvp-backend/app/model"
	"mvp-backend/app/utils"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

func AddArticleCategory(c *gin.Context) {
	var catagory model.ArticleCategories

	//cek is root admin
	role := c.MustGet("role").(int)
	if role != 0 {
		c.JSON(200, gin.H{
			"code":    "403",
			"message": "Not Permit",
			"status":  "success",
		})
		c.Abort()
		return
	}

	catagory.UserID = c.MustGet("user_id").(int)

	if err := c.Bind(&catagory); err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}

	flag, err := model.AddArticleCategory(catagory)
	if flag {
		utils.WrapAPISuccess(c, "success", http.StatusOK)
		return
	} else {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
}

func AllCategory(c *gin.Context) {
	flag, err, category := model.AllCategory()
	if err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusInternalServerError)
		return
	}
	if flag {
		utils.WrapAPIData(c, map[string]interface{}{
			"category": category,
		}, http.StatusOK, "success")
		return
	}
}

func AddArticle(c *gin.Context) {
	var article model.Article

	if err := c.BindJSON(&article); err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusInternalServerError)
		return
	}

	t := time.Now()
	t.String()
	article.CreatedAt = t.Format("2006-01-02 15:04:05")
	article.UserID = c.MustGet("user_id").(int)
	flag, err := model.AddArticle(article)
	if flag {
		utils.WrapAPISuccess(c, "success", http.StatusOK)
		return
	} else {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
}

func LikeArticle(c *gin.Context) {
	var like model.ArticleLikes

	id := c.Param("article")

	articleID, err := strconv.Atoi(id)
	if err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusInternalServerError)
	}

	like.UserID = c.MustGet("user_id").(int)
	like.ArticleID = articleID
	flag, err := model.LikeArticle(like)
	if flag {
		utils.WrapAPISuccess(c, "success", http.StatusOK)
		return
	} else {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
}

func CommentArticle(c *gin.Context) {
	var comment model.ArticleComment

	if err := c.Bind(&comment); err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}

	id := c.Param("article")
	articleID, err := strconv.Atoi(id)
	if err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusInternalServerError)
	}

	t := time.Now()
	t.String()

	comment.CreatedAt = t.Format("2006-01-02 15:04:05")
	comment.UserID = c.MustGet("user_id").(int)
	comment.ArticleID = articleID
	flag, err := model.CommentArticle(comment)
	if flag {
		utils.WrapAPISuccess(c, "success", http.StatusOK)
		return
	} else {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
}

func ReplyCommentArticle(c *gin.Context) {
	var rc model.ReplyArticleComment

	if err := c.Bind(&rc); err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}

	t := time.Now()
	t.String()

	rc.CreatedAt = t.Format("2006-01-02 15:04:05")
	rc.UserID = c.MustGet("user_id").(int)
	flag, err := model.ReplyCommentArticle(rc)
	if flag {
		utils.WrapAPISuccess(c, "success", http.StatusOK)
		return
	} else {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
}

func SelectedArticleCategory(c *gin.Context) {
	userID := c.MustGet("user_id").(int)

	flag, err, res := model.SelectedArticleCategory(userID)
	if flag {
		utils.WrapAPIData(c, map[string]interface{}{
			"data": res,
		}, http.StatusOK, "success")
		return
	} else {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
}

func LikeAndComment(c *gin.Context) {
	id := c.Param("article")

	articleID, err := strconv.Atoi(id)
	if err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusInternalServerError)
	}

	flag, err, res := model.LikeAndComment(articleID)
	if flag {
		utils.WrapAPIData(c, map[string]interface{}{
			"data": res,
		}, http.StatusOK, "success")
		return
	} else {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
}

func ViewFullArticle(c *gin.Context) {
	id := c.Param("article")

	articleID, err := strconv.Atoi(id)
	if err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusInternalServerError)
	}
	//Get Article
	flag, err, article := model.ArticleDetail(articleID)
	if !flag {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
	//Count like n comment
	flag, err, count := model.LikeAndComment(articleID)
	if !flag {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
	//Get Comment n reply
	flag, err, comment := model.Comment(articleID)
	if !flag {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
	utils.WrapAPIData(c, map[string]interface{}{
		"article": article,
		"count":   count,
		"comment": comment,
	}, http.StatusOK, "success")
	return

}

func ViewContentArticle(c *gin.Context) {
	id := c.Param("article")

	articleID, err := strconv.Atoi(id)
	if err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusInternalServerError)
	}
	flag, err, article := model.ArticleDetail(articleID)
	if !flag {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
	utils.WrapAPIData(c, map[string]interface{}{
		"article": article,
	}, http.StatusOK, "success")
	return
}
func ViewCommentArticle(c *gin.Context) {
	id := c.Param("article")

	articleID, err := strconv.Atoi(id)
	if err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusInternalServerError)
	}
	flag, err, comment := model.Comment(articleID)
	if !flag {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
	utils.WrapAPIData(c, map[string]interface{}{
		"comment": comment,
	}, http.StatusOK, "success")
	return
}

func EditArticle(c *gin.Context) {
	var article model.Article

	if err := c.BindJSON(&article); err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusInternalServerError)
		return
	}

	id := c.Param("article")
	articleID, err := strconv.Atoi(id)
	if err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusInternalServerError)
	}

	t := time.Now()
	t.String()
	article.ArticleID = articleID
	article.UpdatedAt = t.Format("2006-01-02 15:04:05")
	article.UserID = c.MustGet("user_id").(int)
	flag, err := model.EditArticle(article)
	if flag {
		utils.WrapAPISuccess(c, "success", http.StatusOK)
		return
	} else {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
}

func ArticleMine(c *gin.Context) {
	userID := c.MustGet("user_id").(int)

	flag, err, res := model.ArticleMine(userID)
	if flag {
		utils.WrapAPIData(c, map[string]interface{}{
			"data": res,
		}, http.StatusOK, "success")
		return
	} else {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
}
