package model

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/pkg/errors"
)

type Article struct {
	ArticleID             int                     `json:"article_id,omitempty"`
	UserID                int                     `json:"user_id,omitempty"`
	ArticleTitle          string                  `json:"article_title,omitempty"`
	Image                 string                  `json:"image,omitempty"`
	ArticleContent        string                  `json:"article_content,omitempty"`
	CreatedAt             string                  `json:"created_at,omitempty"`
	UpdatedAt             string                  `json:"updated_at,omitempty"`
	UserArticleCategories []UserArticleCategories `json:"categories,omitempty"`
}
type ArticlePreview struct {
	UserID            int                 `json:"user_id,omitempty"`
	ArticleID         int                 `json:"article_id,omitempty"`
	FirstName         string              `json:"first_name,omitempty"`
	LastName          string              `json:"last_name,omitempty"`
	ArticleTitle      string              `json:"article_title,omitempty"`
	Image             string              `json:"image,omitempty"`
	ArticleContent    string              `json:"article_content,omitempty"`
	CreatedAt         string              `json:"created_at,omitempty"`
	UpdatedAt         string              `json:"updated_at,omitempty"`
	ArticleCategories []ArticleCategories `json:"article_categories"`
	LikeCount         int                 `json:"like_count,omitempty"`
	CommentCount      int                 `json:"comment_count,omitempty"`
}
type MasterComment struct {
	ArticleCommentID    int                   `gorm:"primary_key" json:"article_comment_id,omitempty"`
	ArticleID           int                   `json:"article_id,omitempty"`
	Comment             string                `json:"comment,omitempty"`
	FirstName           string                `json:"first_name,omitempty"`
	LastName            string                `json:"last_name,omitempty"`
	UserID              int                   `json:"user_id,omitempty"`
	CreatedAt           string                `json:"created_at,omitempty"`
	ReplyArticleComment []ReplyArticleComment `json:"reply_article_comment"`
}
type Category struct {
	ArticleArticleCategories []ArticleArticleCategories `json:"article_article_categories"`
}

type ArticleArticleCategories struct {
	ArticleArticleCategoryID int `gorm:"primary_key" json:"article_category_id,omitempty"`
	ArticleID                int `json:"article_id,omitempty"`
	ArticleCategoryID        int `json:"article_category_id,omitempty"`
}

type ArticleComment struct {
	ArticleCommentID int    `gorm:"primary_key" json:"article_comment_id,omitempty"`
	ArticleID        int    `json:"article_id,omitempty"`
	Comment          string `json:"comment,omitempty"`
	UserID           int    `json:"user_id,omitempty"`
	CreatedAt        string `json:"created_at,omitempty"`
}
type ReplyArticleComment struct {
	ReplyArticleCommentID int    `gorm:"primary_key" json:"reply_article_comment_id,omitempty"`
	ArticleCommentID      int    `json:"article_comment_id,omitempty"`
	UserID                int    `json:"user_id,omitempty"`
	FirstName             string `json:"first_name,omitempty"`
	LastName              string `json:"last_name,omitempty"`
	ReplyComment          string `json:"reply_comment,omitempty"`
	CreatedAt             string `json:"created_at,omitempty"`
}

type ArticleCategories struct {
	ArticleCategoryID   int    `gorm:"primary_key" json:"article_category_id,omitempty"`
	UserID              int    `json:"user_id,omitempty"`
	ArticleCategoryName string `json:"article_category_name,omitempty"`
}
type ArticleLikes struct {
	ArticleLikeID int `gorm:"primary_key" json:"article_like_id,omitempty"`
	ArticleID     int `json:"article_id,omitempty"`
	UserID        int `json:"user_id,omitempty"`
}

func AddArticleCategory(c ArticleCategories) (bool, error) {

	//Insert
	stmt, err := db.Prepare("INSERT INTO `article_categories`(user_id,article_category_name) VALUES (?,?)")
	if err != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}
	_, errr := stmt.Exec(c.UserID, c.ArticleCategoryName)
	if errr != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}
	return true, nil
}

func AllCategory() (bool, error, []ArticleCategories) {
	var categories []ArticleCategories

	rows, err := db.Query("SELECT DISTINCT article_category_id, article_category_name FROM article_categories")
	if err != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err), []ArticleCategories{}
	}
	for rows.Next() {
		var categoy ArticleCategories
		rows.Scan(&categoy.ArticleCategoryID, &categoy.ArticleCategoryName)
		categories = append(categories, categoy)
	}
	defer rows.Close()
	return true, nil, categories
}

func AddArticle(a Article) (bool, error) {

	stmt, err := db.Prepare("INSERT INTO `articles`(user_id, article_title, image, article_content, created_at) VALUES (?,?,?,?,?)")
	if err != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}
	rs, errr := stmt.Exec(a.UserID, a.ArticleTitle, a.Image, a.ArticleContent, a.CreatedAt)
	if errr != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}
	articleID, er := rs.LastInsertId()
	if er != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}
	fmt.Println("last id", articleID)

	//insert article category
	for _, v := range a.UserArticleCategories {
		ac := strings.Replace(strconv.Itoa(v.ArticleCategoryID), "{", "", -1)
		acid := strings.Replace(ac, "}", "", -1)

		stmt, err := db.Prepare("INSERT INTO `article_article_categories`(article_id, article_category_id) VALUES (?,?)")
		if err != nil {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err)
		}

		_, errr := stmt.Exec(articleID, acid)
		if errr != nil {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err)
		}
	}

	return true, nil
}

func LikeArticle(l ArticleLikes) (bool, error) {
	//Cek like
	var count int
	row := db.QueryRow("SELECT COUNT(article_like_id)  FROM article_likes  WHERE user_id=? AND article_id = ?", l.UserID, l.ArticleID)
	row.Scan(&count)

	if count == 0 {
		//If Count > 0 -> Delete
		stmt, err := db.Prepare("INSERT INTO `article_likes` (`article_id`, `user_id`) VALUES (?,?)")
		if err != nil {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err)
		}
		_, errr := stmt.Exec(l.ArticleID, l.UserID)
		if errr != nil {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err)
		}
	} else if count > 0 {
		//If Count = 0 -> Insert
		stmt, err := db.Prepare("DELETE FROM article_likes WHERE user_id=? AND article_id = ?")
		if err != nil {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err)
		}

		_, errr := stmt.Exec(l.UserID, l.ArticleID)
		if errr != nil {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err)
		}
	}

	return true, nil
}

func CommentArticle(ac ArticleComment) (bool, error) {
	stmt, err := db.Prepare("INSERT INTO `article_comments` (`article_id`, `comment`, `user_id`, `created_at`) VALUES (?,?,?,?)")
	if err != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}
	_, errr := stmt.Exec(ac.ArticleID, ac.Comment, ac.UserID, ac.CreatedAt)
	if errr != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}
	return true, nil
}

func ReplyCommentArticle(rc ReplyArticleComment) (bool, error) {
	stmt, err := db.Prepare("INSERT INTO `reply_comments` (`article_comment_id`, `user_id`, `reply_comment`, `created_at`) VALUES (?,?,?,?)")
	if err != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}

	fmt.Println("acid", rc.ArticleCommentID, "uid", rc.UserID, "rc", rc.ReplyComment, "c", rc.CreatedAt)
	_, errr := stmt.Exec(rc.ArticleCommentID, rc.UserID, rc.ReplyComment, rc.CreatedAt)
	if errr != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}
	return true, nil
}

func SelectedArticleCategory(userID int) (bool, error, []ArticlePreview) {
	var articles []ArticlePreview
	var categories []ArticleCategories

	//Basic Information
	articleRows, err := db.Query("SELECT DISTINCT D.article_id, E.first_name, E.Last_name, D.article_title, D.image, D.article_content, D.created_at, D.updated_at FROM user_article_categories A JOIN article_categories B ON A.article_category_id = B.article_category_id JOIN article_article_categories C ON C.article_category_id = B.article_category_id JOIN articles D ON D.article_id = C.article_id JOIN user_accounts E ON E.user_id = D.user_id WHERE A.user_id = ? ORDER BY D.created_at DESC LIMIT 10", userID)
	if err != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err), []ArticlePreview{}
	}
	for articleRows.Next() {
		var a ArticlePreview
		articleRows.Scan(&a.ArticleID, &a.FirstName, &a.LastName, &a.ArticleTitle, &a.Image, &a.ArticleContent, &a.CreatedAt, &a.UpdatedAt)

		//Category
		categoryRows, err := db.Query("SELECT DISTINCT ac.article_category_name FROM article_categories ac JOIN article_article_categories aac ON ac.article_category_id = aac.article_category_id WHERE aac.article_id = ? ORDER BY article_category_name ASC", a.ArticleID)
		if err != nil {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err), []ArticlePreview{}
		}
		for categoryRows.Next() {
			var category ArticleCategories
			categoryRows.Scan(&category.ArticleCategoryName)
			categories = append(categories, category)
		}

		//Like Count
		var like int
		var comment int
		row := db.QueryRow("SELECT (SELECT COUNT(*) FROM article_likes WHERE article_id = ?) AS like_count,(SELECT COUNT(*)  FROM reply_comments X JOIN article_comments Y ON Y.article_comment_id = X.article_comment_id JOIN articles Z ON Z.article_id = Y.article_id WHERE Z.article_id =?) AS comment_count FROM DUAL", a.ArticleID, a.ArticleID)
		row.Scan(&like, &comment)

		article := ArticlePreview{
			ArticleID: a.ArticleID, FirstName: a.FirstName, LastName: a.LastName, ArticleTitle: a.ArticleTitle, Image: a.Image, ArticleContent: a.ArticleContent, CreatedAt: a.CreatedAt, UpdatedAt: a.UpdatedAt,
			ArticleCategories: categories,
			LikeCount:         like, CommentCount: comment,
		}

		categories = []ArticleCategories{} //reset category
		articles = append(articles, article)
	}
	return true, nil, articles
}

func LikeAndComment(artickelID int) (bool, error, ArticlePreview) {
	var lnc ArticlePreview
	row := db.QueryRow("SELECT (SELECT COUNT(*) FROM article_likes WHERE article_id = ?) AS like_count,(SELECT COUNT(*)  FROM reply_comments X JOIN article_comments Y ON Y.article_comment_id = X.article_comment_id JOIN articles Z ON Z.article_id = Y.article_id WHERE Z.article_id =?) AS comment_count FROM DUAL", artickelID, artickelID)
	row.Scan(&lnc.LikeCount, &lnc.CommentCount)

	return true, nil, lnc
}

func ArticleDetail(artickelID int) (bool, error, ArticlePreview) {
	var a ArticlePreview
	var categories []ArticleCategories

	row := db.QueryRow("SELECT DISTINCT a.article_id, u.first_name, u.last_name, a.article_title, a.image, a.article_content, a.created_at, a.updated_at FROM articles a JOIN user_accounts u ON a.user_id = u.user_id WHERE a.article_id = ?", artickelID)
	row.Scan(&a.ArticleID, &a.FirstName, &a.LastName, &a.ArticleTitle, &a.Image, &a.ArticleContent, &a.CreatedAt, &a.UpdatedAt)

	categoryRows, err := db.Query("SELECT DISTINCT ac.article_category_name FROM article_categories ac JOIN article_article_categories aac ON ac.article_category_id = aac.article_category_id WHERE aac.article_id = ? ORDER BY article_category_name ASC", a.ArticleID)
	if err != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err), ArticlePreview{}
	}
	for categoryRows.Next() {
		var category ArticleCategories
		categoryRows.Scan(&category.ArticleCategoryName)
		categories = append(categories, category)
	}
	a.ArticleCategories = categories

	return true, nil, a
}

func Comment(artickelID int) (bool, error, []MasterComment) {
	var comments []MasterComment
	var reply []ReplyArticleComment

	row, err := db.Query("Select DISTINCT ac.article_comment_id, ac.comment, u.first_name, u.last_name, ac.created_at FROM article_comments ac JOIN user_accounts u ON ac.user_id = u.user_id WHERE ac.article_id = ?", artickelID)
	if err != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err), []MasterComment{}
	}
	for row.Next() {
		var c MasterComment
		row.Scan(&c.ArticleCommentID, &c.Comment, &c.FirstName, &c.LastName, &c.CreatedAt)

		//Get Reply
		Rows, err := db.Query("SELECT DISTINCT rc.reply_comment_id, u.first_name, u.last_name, rc.reply_comment, rc.created_at FROM reply_comments rc JOIN user_accounts u ON rc.user_id = u.user_id WHERE rc.article_comment_id = ?", c.ArticleCommentID)
		if err != nil {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err), []MasterComment{}
		}
		for Rows.Next() {
			var r ReplyArticleComment
			Rows.Scan(&r.ReplyArticleCommentID, &r.FirstName, &r.LastName, &r.ReplyComment, &r.CreatedAt)
			reply = append(reply, r)
		}

		comment := MasterComment{
			ArticleCommentID: c.ArticleCommentID, Comment: c.Comment, FirstName: c.FirstName, LastName: c.LastName, CreatedAt: c.CreatedAt,
			ReplyArticleComment: reply,
		}

		reply = []ReplyArticleComment{} //reset reply
		comments = append(comments, comment)
	}

	return true, nil, comments
}

func EditArticle(a Article) (bool, error) {

	stmt, err := db.Prepare("UPDATE `articles` SET `article_title`= ? ,`image`=?,`article_content`=? ,`updated_at`= ? WHERE `article_id` = ? AND `user_id` = ?")
	if err != nil {
		return false, errors.Errorf("NOT PERMIT")
	}
	_, errr := stmt.Exec(a.ArticleTitle, a.Image, a.ArticleContent, a.UpdatedAt, a.ArticleID, a.UserID)
	if errr != nil {
		return false, errors.Errorf("invalid prepare statement1 :%+v\n", err)
	}
	fmt.Println("aid", a.ArticleID, "uid", a.UserID)
	//Delete Category
	stmt, er := db.Prepare("DELETE FROM `article_article_categories` WHERE `article_id` = ?")
	if er != nil {
		return false, errors.Errorf("invalid prepare statement2 :%+v\n", err)
	}

	_, e := stmt.Exec(a.ArticleID)
	if e != nil {
		return false, errors.Errorf("invalid prepare statement3 :%+v\n", err)
	}

	//insert article category
	for _, v := range a.UserArticleCategories {
		ac := strings.Replace(strconv.Itoa(v.ArticleCategoryID), "{", "", -1)
		acid := strings.Replace(ac, "}", "", -1)

		stmt, err := db.Prepare("INSERT INTO `article_article_categories`(article_id, article_category_id) VALUES (?,?)")
		if err != nil {
			return false, errors.Errorf("invalid prepare statement 4:%+v\n", err)
		}

		_, errr := stmt.Exec(a.ArticleID, acid)
		if errr != nil {
			return false, errors.Errorf("invalid prepare statement5 :%+v\n", err)
		}
	}

	return true, nil
}

func ArticleMine(userID int) (bool, error, []ArticlePreview) {
	var articles []ArticlePreview
	var categories []ArticleCategories

	//Basic Information
	articleRows, err := db.Query("SELECT DISTINCT D.article_id, E.first_name, E.Last_name, D.article_title, D.image, D.article_content, D.created_at, D.updated_at FROM user_article_categories A JOIN article_categories B ON A.article_category_id = B.article_category_id JOIN article_article_categories C ON C.article_category_id = B.article_category_id JOIN articles D ON D.article_id = C.article_id JOIN user_accounts E ON E.user_id = D.user_id WHERE D.user_id = ? ORDER BY D.created_at DESC LIMIT 10", userID)
	if err != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err), []ArticlePreview{}
	}
	for articleRows.Next() {
		var a ArticlePreview
		articleRows.Scan(&a.ArticleID, &a.FirstName, &a.LastName, &a.ArticleTitle, &a.Image, &a.ArticleContent, &a.CreatedAt, &a.UpdatedAt)

		//Category
		categoryRows, err := db.Query("SELECT DISTINCT ac.article_category_name FROM article_categories ac JOIN article_article_categories aac ON ac.article_category_id = aac.article_category_id WHERE aac.article_id = ? ORDER BY article_category_name ASC", a.ArticleID)
		if err != nil {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err), []ArticlePreview{}
		}
		for categoryRows.Next() {
			var category ArticleCategories
			categoryRows.Scan(&category.ArticleCategoryName)
			categories = append(categories, category)
		}

		//Like Count
		var like int
		var comment int
		row := db.QueryRow("SELECT (SELECT COUNT(*) FROM article_likes WHERE article_id = ?) AS like_count,(SELECT COUNT(*)  FROM reply_comments X JOIN article_comments Y ON Y.article_comment_id = X.article_comment_id JOIN articles Z ON Z.article_id = Y.article_id WHERE Z.article_id =?) AS comment_count FROM DUAL", a.ArticleID, a.ArticleID)
		row.Scan(&like, &comment)

		article := ArticlePreview{
			ArticleID: a.ArticleID, FirstName: a.FirstName, LastName: a.LastName, ArticleTitle: a.ArticleTitle, Image: a.Image, ArticleContent: a.ArticleContent, CreatedAt: a.CreatedAt, UpdatedAt: a.UpdatedAt,
			ArticleCategories: categories,
			LikeCount:         like, CommentCount: comment,
		}

		categories = []ArticleCategories{} //reset category
		articles = append(articles, article)
	}
	return true, nil, articles
}
