package model

import (
	"fmt"
	"mvp-backend/app/utils"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"

	_ "github.com/go-sql-driver/mysql"
)

type UserAccount struct {
	UserID     int     `gorm:"primary_key" json:"user_id,omitempty"`
	FirstName  string  `json:"first_name,omitempty"`
	LastName   string  `json:"last_name,omitempty"`
	Email      string  `json:"email,omitempty"`
	Password   string  `json:"password,omitempty"`
	Image      *string `json:"image,omitempty"`
	Role       int     `json:"role,omitempty"`
	Phone      *string `json:"phone,omitempty"`
	Address    *string `json:"address,omitempty"`
	Bio        *string `json:"bio,omitempty"`
	BOD        *string `json:"bod,omitempty"`
	IsVerified int     `json:"is_verified,omitempty"`
	Flag       int     `json:"flag,omitempty"`
	CreatedAt  string  `json:"created_at,omitempty"`
	UpdatedAt  string  `json:"updated_at,omitempty"`
	IsActive   int     `json:"is_active,omitempty"`
}

type UserMany struct {
	Users []Users `json:"users"`
}

type Users struct {
	UserID int `json:"user_id,omitempty"`
}
type ChooseCategory struct {
	UserID                int                     `json:"user_id,omitempty"`
	UserArticleCategories []UserArticleCategories `json:"user_article_categories"`
}

type Filter struct {
	FilterName string `json:"filter_name,omitempty"`
	FilterID   int    `json:"filter_id,omitempty"`
}
type Password struct {
	UserID          int    `json:"user_id,omitempty"`
	NewPassword     string `json:"new_password,omitempty"`
	ConfirmPassword string `json:"confirm_password,omitempty"`
	OldPassword     string `json:"old_password,omitempty"`
}

type UserArticleCategories struct {
	UserArticleCategoryID int `gorm:"primary_key" json:"user_article_category_id,omitempty"`
	UserID                int `json:"user_id,omitempty"`
	ArticleCategoryID     int `json:"article_category_id"`
}

func AccountProfile(a UserAccount) (bool, error, UserAccount) {

	var acc UserAccount

	row := db.QueryRow("SELECT DISTINCT user_id, first_name, last_name, email, image, role, phone, address, bod,  bio, is_verified, created_at, updated_at, is_active  FROM user_accounts WHERE user_id=?", a.UserID)
	err := row.Scan(&acc.UserID, &acc.FirstName, &acc.LastName, &acc.Email, &acc.Image, &acc.Role, &acc.Phone, &acc.Address, &acc.BOD, &acc.Bio, &acc.IsVerified, &acc.CreatedAt, &acc.UpdatedAt, &acc.IsActive)
	if err != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err), UserAccount{}
	}

	return true, nil, acc
}

func GetCategoryID(u UserAccount) (bool, error, []UserArticleCategories) {
	var categories []UserArticleCategories

	rows, err := db.Query("SELECT DISTINCT article_category_id FROM user_article_categories WHERE user_id=?", u.UserID)
	if err != nil {
		return false, nil, categories
	}
	for rows.Next() {
		var categorie UserArticleCategories
		rows.Scan(&categorie.ArticleCategoryID)
		categories = append(categories, categorie)
	}
	defer rows.Close()

	return true, nil, categories
}

func FilterAccount(f Filter) (bool, error, []UserAccount) {
	var query string
	var img string
	var accounts []UserAccount

	if f.FilterID == 1 {
		img = "(`image` IS NOT NULL)"
	} else {
		img = "(`image` IS NULL)"
	}

	if f.FilterID != 1 && f.FilterID != 0 {
		return false, errors.Errorf("invalid prepare statement :%+v\n"), []UserAccount{}
	}
	if f.FilterName == "deleted" {
		query = "SELECT DISTINCT user_id, first_name, last_name, email, role, is_verified, created_at, updated_at, is_active FROM `user_accounts` WHERE `is_active`= " + strconv.Itoa(f.FilterID)
	} else if f.FilterName == "completed" {
		query = "SELECT DISTINCT user_id, first_name, last_name, email, role, is_verified, created_at, updated_at, is_active FROM user_accounts WHERE  `is_verified` = 1 AND " + img
	} else if f.FilterName == "verified" {
		query = "SELECT DISTINCT user_id, first_name, last_name, email, role, is_verified, created_at, updated_at, is_active FROM `user_accounts` WHERE `is_verified`= " + strconv.Itoa(f.FilterID)
	} else {
		return false, errors.Errorf("invalid prepare statement :%+v\n"), []UserAccount{}
	}

	fmt.Println(query)
	rows, err := db.Query(query)
	if err != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err), []UserAccount{}
	}

	for rows.Next() {
		var a UserAccount
		rows.Scan(&a.UserID, &a.FirstName, &a.LastName, &a.Email, &a.Role, &a.IsVerified, &a.CreatedAt, &a.UpdatedAt, &a.IsActive)
		accounts = append(accounts, a)
	}
	defer rows.Close()

	return true, nil, accounts
}

func EditAccount(u UserAccount) (bool, error) {
	//Update
	stmt, err := db.Prepare("UPDATE `user_accounts` SET `first_name` = ?, `last_name` = ?, `email`=?, `image`=?,  `phone` = ?, `address`= ?, `bod` =?,  `bio` = ?, `updated_at`=? WHERE `user_id` = ?")
	if err != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}

	t := time.Now()
	t.String()

	rs, err := stmt.Exec(u.FirstName, u.LastName, u.Email, u.Image, u.Phone, u.Address, u.BOD, u.Bio, t.Format("2006-01-02 15:04:05"), u.UserID)
	if err != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}

	_, er := rs.RowsAffected()
	if er != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}

	return true, nil
}

func CheckEmail(u UserAccount, ctx string) (bool, error) {
	var count int

	if ctx == "edit" {
		row := db.QueryRow("SELECT COUNT(`email`) FROM `user_accounts` WHERE `email` = ? AND NOT `user_id` = ? ", u.Email, u.UserID)
		err := row.Scan(&count)
		if err != nil {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err)
		}
		if count > 0 {
			return false, nil
		}
	} else if ctx == "validate" {
		row := db.QueryRow("SELECT COUNT(`email`) FROM `user_accounts` WHERE `email` = ? ", u.Email)
		err := row.Scan(&count)
		if err != nil {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err)
		}
		if count == 0 {
			return false, nil
		}
	}

	return true, nil
}

func AccountVerification(u UserAccount) (bool, error) {
	//Cek Status Verification
	var cek int
	var status int
	row := db.QueryRow("SELECT  is_verified FROM user_accounts WHERE user_id =?", u.UserID)
	err := row.Scan(&cek)
	if err != nil {
		return false, errors.Errorf("Account not found")
	}

	if cek == 1 {
		status = 0
	} else {
		status = 1
	}

	stmt, err := db.Prepare("UPDATE `user_accounts` SET `is_verified` = ? WHERE `user_id` = ?")
	if err != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}

	rs, err := stmt.Exec(status, u.UserID)
	if err != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}

	_, er := rs.RowsAffected()
	if er != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}

	return true, nil
}

func AllAccount() (bool, error, []UserAccount) {
	var users []UserAccount

	rows, err := db.Query("SELECT DISTINCT user_id, first_name, last_name, email, role, is_verified, created_at, updated_at FROM user_accounts")
	if err != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err), []UserAccount{}
	}
	for rows.Next() {
		var user UserAccount
		rows.Scan(&user.UserID, &user.FirstName, &user.LastName, &user.Email, &user.Role, &user.IsVerified, &user.CreatedAt, &user.UpdatedAt)
		users = append(users, user)
	}
	defer rows.Close()

	return true, nil, users
}

func VerificationMany(d UserMany) (bool, error) {

	for _, v := range d.Users {
		// fmt.Println(a, v)

		one := strings.Replace(strconv.Itoa(v.UserID), "{", "", -1)
		two := strings.Replace(one, "}", "", -1)

		stmt, err := db.Prepare("UPDATE `user_accounts` SET `is_verified` = ? WHERE `user_id` = ?")
		if err != nil {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err)
		}

		rs, err := stmt.Exec(1, two)
		if err != nil {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err)
		}

		_, er := rs.RowsAffected()
		if er != nil {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err)
		}

	}

	return true, nil
}

func ChangePassword(pass Password) (bool, error) {
	var u UserAccount

	row := db.QueryRow("SELECT password FROM user_accounts WHERE user_id=?", pass.UserID)
	err := row.Scan(&u.Password)
	if err != nil {
		return false, errors.Errorf("Account not found")
	}

	errpass := utils.HashComparator([]byte(u.Password), []byte(pass.OldPassword))
	fmt.Println("passsss", errpass)
	if errpass != nil {
		return false, errors.Errorf("Incorrect Old Password")
	} else {

		hashPass, err := utils.HashGenerator(pass.NewPassword)
		if err != nil {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err)
		}

		stmt, err := db.Prepare("UPDATE `user_accounts` SET `password` = ? WHERE `user_id` = ?")
		if err != nil {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err)
		}

		rs, err := stmt.Exec(hashPass, pass.UserID)
		if err != nil {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err)
		}

		_, er := rs.RowsAffected()
		if er != nil {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err)
		}
	}

	return true, nil
}

func DeleteAccount(u UserAccount) (bool, error) {
	row := db.QueryRow("SELECT user_id FROM user_accounts WHERE user_id=?", u.UserID)
	err := row.Scan(&u.UserID)
	if err != nil {
		return false, errors.Errorf("Account not found")
	}

	stmt, err := db.Prepare("UPDATE `user_accounts` SET `is_active` = ?  WHERE `user_id` = ?")
	if err != nil {
		return false, errors.Errorf("Accout Not Found")
	}

	t := time.Now()
	t.String()

	rs, err := stmt.Exec(0, u.UserID)
	if err != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}

	_, er := rs.RowsAffected()
	if er != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}

	return true, nil
}

func ChooseUserCategory(cc ChooseCategory) (bool, error) {
	for _, v := range cc.UserArticleCategories {
		aid := strings.Replace(strconv.Itoa(v.ArticleCategoryID), "{", "", -1)
		articleid := strings.Replace(aid, "}", "", -1)

		stmt, err := db.Prepare("INSERT INTO `user_article_categories`( `user_id`, `article_category_id`) VALUES (?,?)")
		if err != nil {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err)
		}

		rs, err := stmt.Exec(cc.UserID, articleid)
		if err != nil {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err)
		}

		_, er := rs.RowsAffected()
		if er != nil {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err)
		}

	}

	return true, nil
}
