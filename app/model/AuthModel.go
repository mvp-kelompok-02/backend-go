package model

import (
	"mvp-backend/app/utils"
	"strconv"

	"github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"
)

type Portofolio struct {
	PortofolioID     int    `gorm:"primary_key" json:"portofolio_id,omitempty"`
	UserID           int    `json:"user_id,omitempty"`
	PotofolioContent string `json:"portofolio_content,omitempty"`
	PotofolioTitle   string `json:"portofolio_title,omitempty"`
}

type Showcase struct {
	ShowcaseID    int    `gorm:"primary_key" json:"showcase_id,omitempty"`
	GroupMemberID int    `json:"group_member_id,omitempty"`
	Image         string `json:"image,omitempty"`
	Link          string `json:"link,omitempty"`
	Description   string `json:"description,omitempty"`
	File          string `json:"file,omitempty"`
}

type ShowcaseComment struct {
	ShowcaseCommentID int    `gorm:"primary_key" json:"showcase_comment_id,omitempty"`
	ShowcaseID        int    `json:"showcase_id,omitempty"`
	UserID            int    `json:"user_id,omitempty"`
	Comment           string `json:"comment,omitempty"`
}

type Task struct {
	TaskID        int    `gorm:"primary_key" json:"task_id,omitempty"`
	GroupMemberID int    `json:"group_member_id,omitempty"`
	GroupID       int    `json:"group_id,omitempty"`
	Task          string `json:"task,omitempty"`
	CreatedAt     string `json:"created_at,omitempty"`
	UpdatedAt     string `json:"updated_at,omitempty"`
}

type TaskComment struct {
	TaskCommentID int    `gorm:"primary_key" json:"task_comment_id,omitempty"`
	TaskContentID int    `json:"task_content_id,omitempty"`
	Comment       string `json:"comment,omitempty"`
	GroupMemberID int    `json:"group_member_id,omitempty"`
}

//OK
type Auth struct {
	Email    string `json:"email,omitempty"`
	Password string `json:"password,omitempty"`
}

type SignUp struct {
	FirstName       string `json:"first_name,omitempty"`
	LastName        string `json:"last_name,omitempty"`
	Email           string `json:"email,omitempty"`
	Password        string `json:"password,omitempty"`
	ConfirmPassword string `json:"confirm_password,omitempty"`
	Role            int    `json:"role,omitempty"`
	IsVerified      int    `json:"is_verified,omitempty"`
	Flag            int    `json:"flag,omitempty"`
	CreatedAt       string `json:"created_at,omitempty"`
}

func Register(u SignUp) (bool, error, string) {

	//Insert
	stmt, err := db.Prepare("INSERT INTO `user_accounts`(first_name, last_name, email, password, role, is_verified, flag, created_at, is_active) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)")
	if err != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err), ""
	}
	rs, err := stmt.Exec(u.FirstName, u.LastName, u.Email, u.Password, u.Role, u.IsVerified, u.Flag, u.CreatedAt, 1)
	if err != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err), ""
	}
	userID, er := rs.LastInsertId()
	if er != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err), ""
	}

	return true, nil, strconv.FormatInt(userID, 10)
}

func Registered(u SignUp) bool {
	var count int
	row := db.QueryRow("SELECT COUNT(`email`) FROM `user_accounts` WHERE `email` = ?", u.Email)
	row.Scan(&count)

	if count > 0 {
		return false
	}
	return true
}

func Login(auth Auth) (bool, error, string, string, string) {
	var u UserAccount

	row := db.QueryRow("SELECT DISTINCT user_id,  password, role, is_verified  FROM user_accounts WHERE email=?", auth.Email)
	err := row.Scan(&u.UserID, &u.Password, &u.Role, &u.IsVerified)
	if err != nil {
		return false, errors.Errorf("Account not found"), "", "", ""
	}

	errpass := utils.HashComparator([]byte(u.Password), []byte(auth.Password))
	if errpass != nil {
		return false, errors.Errorf("Incorrect Password"), "", "", ""
	} else {
		sign := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
			"email":   auth.Email,
			"user_id": u.UserID,
			"role":    u.Role,
		})

		token, err := sign.SignedString([]byte("secret"))
		if err != nil {
			return false, err, "", "", ""
		}
		return true, nil, token, strconv.Itoa(u.Role), strconv.Itoa(u.IsVerified)
	}
}
