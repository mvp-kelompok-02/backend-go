package model

import (
	"database/sql"
)

var db *sql.DB

func init() {
	var err error

	db, err = sql.Open("mysql", "admin:sabotage@tcp(sabotage-db)/digitalent_mvp?parseTime=true")
	// db, err = sql.Open("mysql", "root:@tcp(127.0.0.1:3306)/digitalent_mvp?parseTime=true")

	if err != nil {
		panic("failede to connect to database" + err.Error())
	}
}
