package model

import (
	"strings"

	"github.com/pkg/errors"
)

type GroupMember struct {
	GroupMemberID   int               `gorm:"primary_key" json:"group_member_id,omitempty"`
	GroupID         int               `json:"group_id,omitempty"`
	CreatedAt       string            `json:"created_at,omitempty"`
	GroupMemberUser []GroupMemberUser `json:"group_member,omitempty"`
}

type GroupMemberUser struct {
	UserID    int    `json:"user_id,omitempty"`
	Email     string `json:"email,omitempty"`
	FirstName string `json:"first_name,omitempty"`
	LastName  string `json:"last_name,omitempty"`
}

type GroupPI struct {
	GroupID   int    `gorm:"primary_key" json:"group_id,omitempty"`
	UserID    int    `json:"user_id,omitempty"`
	Name      string `json:"name,omitempty"`
	CreatedAt string `json:"created_at,omitempty"`
	UpdatedAt string `json:"updated_at,omitempty"`
}

func AddGroup(g GroupPI) (bool, error) {

	//Insert
	stmt, err := db.Prepare("INSERT INTO `group_pis`(user_id, name, created_at) VALUES (?,?,?)")
	if err != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}
	_, errr := stmt.Exec(g.UserID, g.Name, g.CreatedAt)
	if errr != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}
	return true, nil
}

func AddGroupMember(g GroupMember) (bool, error) {
	var userID string
	for _, v := range g.GroupMemberUser {
		e := strings.Replace(v.Email, "{", "", -1)
		email := strings.Replace(e, "}", "", -1)

		//Get userID from email
		row := db.QueryRow("SELECT DISTINCT user_id FROM user_accounts WHERE email = ?", email)
		err := row.Scan(&userID)

		//Add to group
		stmt, err := db.Prepare("INSERT INTO `group_members`(group_id, user_id, created_at) VALUES (?,?,?)")
		if err != nil {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err)
		}
		_, errr := stmt.Exec(g.GroupID, userID, g.CreatedAt)
		if errr != nil {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err)
		}

	}
	return true, nil
}
