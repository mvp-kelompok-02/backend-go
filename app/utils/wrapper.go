package utils

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

//Body = Error
func WrapAPIError(c *gin.Context, message string, code int) {
	c.JSON(code, map[string]interface{}{
		"code":          code,
		"error_type":    http.StatusText(code),
		"error_details": message,
	})
}

// Success Message Only
func WrapAPISuccess(c *gin.Context, message string, code int) {
	c.JSON(code, map[string]interface{}{
		"code":   code,
		"status": message,
	})
}

//Success message n Output data
func WrapAPIData(c *gin.Context, data interface{}, code int, message string) {
	c.JSON(code, map[string]interface{}{
		"code":   code,
		"status": message,
		"data":   data,
	})
}
