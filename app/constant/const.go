package constant

const (
	ADMIN_ROOT = 0
	ADMIN      = 1
	MEMBER     = 2
	NO         = 0
	YES        = 1
	ISVERIFIED = 0
	NORMAL     = 0
	ISACTIVE   = 1
)
