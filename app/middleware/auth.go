package middleware

import (
	"fmt"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/mitchellh/mapstructure"
)

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}

func Auth(c *gin.Context) {
	tokenString := c.Request.Header.Get("Authorization")
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if jwt.GetSigningMethod("HS256") != token.Method {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return []byte("secret"), nil
	})

	if token != nil && err == nil {
		fmt.Println("token verified")
		claims := token.Claims.(jwt.MapClaims)
		fmt.Println(claims)
		var userID int
		var userRole int
		errId := mapstructure.Decode(claims["user_id"], &userID)
		if errId != nil {
			result := gin.H{
				"message": err.Error(),
			}
			c.JSON(http.StatusUnauthorized, result)
			c.Abort()
		}
		errRole := mapstructure.Decode(claims["role"], &userRole)
		if errRole != nil {
			result := gin.H{
				"message": err.Error(),
			}
			c.JSON(http.StatusUnauthorized, result)
			c.Abort()
		}

		c.Set("user_id", userID)
		c.Set("role", userRole)
	} else {
		result := gin.H{
			"message": "token tidak valid",
			"error":   err.Error(),
		}
		c.JSON(http.StatusUnauthorized, result)
		c.Abort()
	}

}
