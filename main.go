package main

import (
	"mvp-backend/app/controller"
	"mvp-backend/app/middleware"

	"github.com/gin-gonic/gin"
)

func main() {

	router := gin.Default()
	router.LoadHTMLGlob("./app/templates/*")
	router.Use(middleware.CORSMiddleware())
	base := router.Group("")
	{
		//Social media or OAuth

		//Normal Login n Register
		base.POST("", controller.Index)                  //login
		base.POST("/auth/register", controller.Register) //registrasi by email
		base.POST("/auth/login", controller.Login)       //login

		//Verification User

		//Account
		account := base.Group("/account")
		{
			account.GET("", middleware.Auth, controller.AccountProfile)                             //melihat profile dari user yang sedang login
			account.PUT("/edit", middleware.Auth, controller.EditAccount)                           //edit profile user yang sedang login
			account.PUT("/password", middleware.Auth, controller.ChangePassword)                    //mengganti password user yang sedang login
			account.POST("/article", middleware.Auth, controller.ChooseUserCategory)                //user memilih artikel kategori yang dikehendaki
			account.GET("/all", middleware.Auth, controller.AllAccount)                             //admin dapat melihat semua user
			account.GET("/filter/:filter/:id", middleware.Auth, controller.FilterAccount)           //admin mengambil data user dengan filter -> masih aktif, sudah verifikasi email, data lengkap
			account.GET("/verification/email/:id", controller.AccountEmailVerification)             //verifikasi dari emil
			account.PUT("/verification/admin/:id", middleware.Auth, controller.AccountVerification) //verifikasi satu akun oleh admin
			account.PUT("/verification/many", middleware.Auth, controller.VerificationMany)         //verifikasi banyak akun oleh admin
			account.PUT("/delete/:id", middleware.Auth, controller.DeleteAccount)                   //hapus akun oleh admin
			account.POST("/validate/email", middleware.Auth, controller.ValidateEmail)              //hapus akun oleh admin
		}

		//Article
		article := base.Group("/article")
		{
			article.POST("/category/add", middleware.Auth, controller.AddArticleCategory)              //menambahkan kategori artikel
			article.GET("/categories", middleware.Auth, controller.AllCategory)                        //mengambil semua kategori artikel
			article.POST("/create", middleware.Auth, controller.AddArticle)                            //membuat artikel baru
			article.PUT("/edit/:article", middleware.Auth, controller.EditArticle)                     //edit artikel
			article.GET("/view/full/:article", middleware.Auth, controller.ViewFullArticle)            //melihat artikel, comment, reply comment, jumlah comment dan like
			article.GET("/view/content/:article", middleware.Auth, controller.ViewContentArticle)      //melihat konten utama dari sebuah artikel
			article.GET("/view/comment/:article", middleware.Auth, controller.ViewCommentArticle)      //melihat komentar dan reply komentar dari sebuah artikel
			article.GET("/view/like-and-comment/:article", middleware.Auth, controller.LikeAndComment) //melihat jumlah comment dan like dari sebuah artikel
			article.GET("/view/selected", middleware.Auth, controller.SelectedArticleCategory)         //mengambil artikel sesuai kategori pilihan user
			article.GET("/view/mine", middleware.Auth, controller.ArticleMine)                         //menampilkan list artikel artickel user yang login
			article.POST("/like/:article", middleware.Auth, controller.LikeArticle)                    //like atau dislike sebuah artikel
			article.POST("/comment/:article", middleware.Auth, controller.CommentArticle)              //mengomentari artikel
			article.POST("/reply", middleware.Auth, controller.ReplyCommentArticle)                    //membalas komentar
		}

		//Group
		group := base.Group("/group")
		{
			group.POST("/create", middleware.Auth, controller.AddGroup)               //membuat group baru
			group.POST("/member/add/:id", middleware.Auth, controller.AddGroupMember) //membuat group baru

		}

	}
	router.Run(":4121")
}
